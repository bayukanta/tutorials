extends KinematicBody2D

export (int) var speed = 200
export (int) var dash_speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600

const UP = Vector2(0,-1)

var velocity = Vector2()

func get_input():
	velocity.x = 0
	if Input.is_action_just_pressed('up'): #kalau is_on_floor() dihapus maka dia bisa jump di udara
		velocity.y = jump_speed
	if Input.is_action_pressed('right'):
		velocity.x += speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed
	if Input.is_action_pressed('right') and Input.is_key_pressed(KEY_SHIFT): #kalau dipencet arah dan shift maka akan dash
		velocity.x += dash_speed
	if Input.is_action_pressed('left') and Input.is_key_pressed(KEY_SHIFT): 
		velocity.x -= dash_speed

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
